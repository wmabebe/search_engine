"""
	Author: Waqwoya Abebe
	Date: Fri, Sep 28, 2018
	Com-S 572 Fall 2018
"""
import sys
import re
from difflib import SequenceMatcher
from fuzzywuzzy import fuzz,process
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
import requests
from requests.exceptions import HTTPError
import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning

class Node():
	directory = ""
	query = ""
	location = "local"
	def __init__(self,url,text,parent):
		self.url = url
		self.text = text
		self.parent = parent
		self.heu = 0

	def getText(self):
		return self.text

	def getURL(self):
		return self.url

	def get_contents(self):
		try:
			with open(Node.directory + self.url,"r") as file:
				page = file.read().replace('\n', '')
				return page
		except IOError:
			return None

	def get_web_children(self):
		try:
			print ("OPEN: " + self.url)
			#page = urlopen(self.url)
			page = urlopen(Request(self.url, headers={'User-Agent': 'Mozilla'}))
			soup = BeautifulSoup(page, 'html.parser')
			children = []
			for link in soup.findAll('a'):
				if link.get("href") and \
				(link.get("href").startswith("http:") or link.get("href").startswith("https:")) and \
				not link.get("href").endswith(".pdf") and \
				"wiki" in link.get("href"):
					child = Node(link.get("href"),link.text,self)
					children.append(child)
			return children
		except HTTPError:
			pass
		return []

	def is_goal(self,query):
		if Node.location == "local":
			page = self.get_contents()
			if page == None:
				return page
			return bool(re.search(query,page))
		else:
			try:
				print ("Is goal: " + self.url)
				r = requests.get(self.url)
				return bool(re.search(query,r.text))
			except HTTPError:
				print (self.url + " failed to open!")
				return False

	def get_children(self):
		page = self.get_contents()
		if page == None:
			print (url + " doesn't exist!")
			sys.exit(1)
		children = [Node(url.split(' > ')[0],url.split(' > ')[1],self) for url in re.findall(r'<A HREF = (.*?) </A>', page)]
		return children

	def ancestry(self):
		if self.parent == None:
			return [self]
		return self.parent.ancestry() + [self]

	def set_heuristic(self,query):
		#self.heu = SequenceMatcher(None,self.text,query).ratio() if self.text else 0
		self.heu = fuzz.token_sort_ratio(self.text,query)

	def tuplize(self):
		return (self.url,self.heu)

	def __str__(self):
		return self.url

	def __repr__(self):
		return (self.url)

	def __eq__(self,obj):
		return isinstance(obj,Node) and self.url == obj.url

	def __gt__(self,obj):
		return self.heu >= obj.heu

def search(root,query,algo,optimize,prune,verbose):
	if root.is_goal(query) == None:
		print (root.getURL() + " doesn't exist!")
		sys.exit(1)
	elif root.is_goal(query):
		return root
	print ("\n---- Algo: " + algo.upper() + " ----")
	print ("---- Query: " + query + " ----\n")
	if optimize: print ("---- Optimized  ----")
	if prune != -1: print ("---- Prune 20  ----\n")
	closed_list = []
	open_list = []
	open_list.append(root)
	count = 1
	while open_list:
		if optimize:
			node = max(open_list)
			open_list.remove(node)
		else:
			node = open_list.pop() if algo == "dfs" else open_list.pop(0)
		closed_list.append(node)
		if node.is_goal(query):
			print (closed_list)
			print ("\n---- " + str(len(closed_list)) + " urls visited ----")
			return node
		children = node.get_web_children() if Node.location == "global" else node.get_children()
		for child in children:
			child.set_heuristic(query)
			if child not in closed_list and child not in open_list:
				open_list.append(child)
		if prune != -1:
			open_list.sort(key=lambda node:node.heu,reverse=True)
			open_list = open_list[:prune]
			if verbose:
				print ("Itr "+ str(count) + ": " +str(len(open_list)) + " nodes being tracked")
				for n in open_list:
					print (str(n.tuplize()),end=":")
				print ("\n")
		count += 1

	return None

def main():
	if len(sys.argv) < 3:
		print ("Please use: python3 web_search.py URL QUERY [ALGO] [LOCAL] [OPTIMIZE] [PRUNE]")
		print ("Eg.")
		print ("URL: intranet1/page1.html")
		print ("QUERY: \"Query1 Query2 Query3\"")
		print ("[ALGO]: bfs, dfs")
		print ("[OPTIMIZE]: -o")
		print ("[PRUNE]: -p")
		print ("[LOCAL]: -l")
		sys.exit(1)
	url = path = sys.argv[1]
	if not url.startswith("http"):
		Node.directory = path[0:path.rfind("/")] + "/"
		url = path[path.rfind("/") + 1:]
		urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
		requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

	else:
		Node.location = "global"
	Node.query = query = sys.argv[2]
	algo = sys.argv[3]
	optimize = True if "-o" in sys.argv else False
	prune = 20 if "-p" in sys.argv else -1
	verbose = True if "-v" in sys.argv else False

	if "bfs" in sys.argv or "dfs" in sys.argv or optimize:
		if "bfs" in sys.argv:
			algo = "bfs"
		elif "dfs" in sys.argv:
			algo = "dfs"
		else:
			algo = "Best First Search"
		root = Node(url,"",None)
		root.set_heuristic(query)
		result = search(root,query,algo,optimize,prune,verbose)
		print ("\n#### Backtracking ####")
		print ("#### "+ Node.directory +" ####\n")
		for node in result.ancestry():
			print (str(node) + " ---> ",end="")
		print (u"\u25A0\n\n" + str(len(result.ancestry())) + " nodes to root", end = "\n\n")

	else:
		print ("Please use query options: bfs, dfs, -o")

if __name__ == '__main__':
	main()